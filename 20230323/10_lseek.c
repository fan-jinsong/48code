#include <48func.h>
int main(int argc, char *argv[])
{
    // ./10_lseek file1
    ARGS_CHECK(argc,2);
    int fd = open(argv[1],O_RDWR);
    ERROR_CHECK(fd,-1,"open");
    char buf[1024] = {0};
    read(fd,buf,sizeof(buf));
    off_t oret = lseek(fd,0,SEEK_SET);//将当前读写位置移动到开始
    ERROR_CHECK(oret,-1,"lseek");
    buf[4] = 'O';
    write(fd,buf,strlen(buf));
    close(fd);
    return 0;
}

