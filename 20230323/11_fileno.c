#include <48func.h>
int main(int argc, char *argv[])
{
    // ./11_fileno file1
    ARGS_CHECK(argc,2);
    FILE *fp = fopen(argv[1],"r+");//创建一个文件流
    ERROR_CHECK(fp,NULL,"fopen");
    //write(3,"hello",5);//使用文件描述符写入
    //write(fp->_fileno,"world",5);
    write(fileno(fp),"hello",5);//面向接口编程，而不是面向实现编程
    fclose(fp);
    return 0;
}

