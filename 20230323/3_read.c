#include <48func.h>
int main(int argc, char *argv[])
{
    // ./3_read file1
    ARGS_CHECK(argc,2);
    int fd = open(argv[1], O_RDONLY);
    ERROR_CHECK(fd,-1,"open");
    //char buf[6] = {0};// 申请了6个字节
    //ssize_t sret = read(fd,buf,5); // 读取上限时5个字节
    //char buf[1024] = {0};// 申请了1024个字节
    //ssize_t sret = read(fd,buf,1023); // 读取上限时1023个字节
    char buf[6] = {0};// 申请了6个字节
    ssize_t sret = read(fd,buf,4); // 读取上限时5个字节
    ERROR_CHECK(sret,-1,"read");
    printf("sret = %ld\n", sret);
    printf("buf = %s\n", buf);
    sret = read(fd,buf,2); // 读取上限时2个字节
    ERROR_CHECK(sret,-1,"read");
    printf("sret = %ld\n", sret);
    printf("buf = %s\n", buf);
    close(fd);
    return 0;
}

