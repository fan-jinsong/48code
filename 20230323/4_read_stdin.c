#include <48func.h>
int main()
{
    // stdin --> 0
    char buf[1024] = {0};
    ssize_t sret = read(0,buf,sizeof(buf)-1);
    ERROR_CHECK(sret,-1,"read");
    printf("sret = %ld, buf = %s\n", sret, buf);
    return 0;
}

