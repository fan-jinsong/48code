#include <48func.h>
int main(int argc, char *argv[])
{
    // ./6_cp src dest
    ARGS_CHECK(argc,3);
    int fdr = open(argv[1],O_RDONLY);
    ERROR_CHECK(fdr,-1,"open fdr");
    int fdw = open(argv[2],O_WRONLY|O_CREAT, 0666);
    ERROR_CHECK(fdw,-1,"open fdw");
    char buf[4096] = {0}; // char不是字符的意思，希望用长度为1字节的数据
    ssize_t sret = read(fdr,buf,sizeof(buf));
    //write(fdw,buf,sizeof(buf));
    //write(fdw,buf,strlen(buf));
    write(fdw,buf,sret);
    close(fdw);
    close(fdr);
    return 0;
}

