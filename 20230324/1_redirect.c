#include <48func.h>
int main(int argc, char *argv[])
{
    // ./1_direct file1
    ARGS_CHECK(argc,2);
    int oldfd = open(argv[1],O_RDWR);
    ERROR_CHECK(oldfd,-1,"open");
    // 此时printf显示在屏幕上
    printf("我过来啦！\n");
    // 用4号来备份stdout设备
    int newfd = 4;
    dup2(STDOUT_FILENO,newfd);
    // 重定向到文件中
    dup2(oldfd,STDOUT_FILENO);
    printf("我过去啦！\n");
    // 重定向回到stdout设备
    dup2(newfd,STDOUT_FILENO);
    printf("我又过来啦！\n");
    close(oldfd);
    return 0;
}

