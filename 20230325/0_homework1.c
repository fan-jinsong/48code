//编写C程序，判断任意两个文件，其内容是否是完全一致的。
#include <48func.h>
int main(int argc, char *argv[])
{
    // ./0_homework1 file1 file2
    ARGS_CHECK(argc,3);
    int fd1 = open(argv[1],O_RDONLY);
    ERROR_CHECK(fd1,-1,"open fd1");
    int fd2 = open(argv[2],O_RDONLY);
    ERROR_CHECK(fd2,-1,"open fd2");
    //char ch1,ch2;
    char buf1[4096];
    char buf2[4096];
    while(1){
        //ssize_t sret1 = read(fd1,&ch1,1);
        //ssize_t sret2 = read(fd2,&ch2,1);
        ssize_t sret1 = read(fd1,buf1,4096);
        ssize_t sret2 = read(fd2,buf2,4096);
        if(sret1 != sret2){
            printf("Not the same!\n");
            break;
        }
        for(int i = 0; i < sret1; ++i){
            if(buf1[i] != buf2[i]){
                printf("Not the same!\n");
                break;
            }
        }
        if(sret1 == 0){
            printf("The same!\n");
            break;
        }
        // if(ch1 != ch2){
        //     printf("not the same!\n");
        //     break;
        // }
        // if(sret1 == 0 || sret2 == 0){
        //     if(sret1 != sret2){
        //         printf("not the same!\n");
        //         break;
        //     }
        //     else{
        //         printf("the same!\n");
        //         break;
        //     }
        // }
    }
    return 0;
}

