#include <48func.h>
typedef struct student_s{ 
    int num;
    char name[20];
    float score;
} student_t;
int main(int argc, char *argv[])
{
    // ./2_homework3 file1
    ARGS_CHECK(argc,2);
    student_t s[3] = {
        {1001, "Caixukun", 88.8},
        {1003, "Wuyifan", 59.9},
        {1005, "Liyifeng", 60}
    };
    int fd = open(argv[1],O_RDWR);
    ERROR_CHECK(fd,-1,"open");
    write(fd,s,sizeof(s));
    lseek(fd,0,SEEK_SET);
    student_t valFromFile[3];
    read(fd,valFromFile,sizeof(valFromFile));
    for(int i = 0; i < 3; ++i){
        printf("%d %s %.2f\n", valFromFile[i].num, valFromFile[i].name, valFromFile[i].score);
    }
    return 0;
}

