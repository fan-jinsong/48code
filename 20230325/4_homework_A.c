#include <48func.h>
int main(int argc, char *argv[])
{
    // ./4_homework_A 1.pipe
    ARGS_CHECK(argc,2);
    int fdw = open(argv[1],O_WRONLY);
    ERROR_CHECK(fdw,-1,"open");
    
    printf("Hello World 1\n");
    int newfd = 10;
    // 备份stdout设备文件对象
    dup2(STDOUT_FILENO,newfd);
    // 1号文件描述符引用管道的写端
    dup2(fdw,STDOUT_FILENO);
    printf("Hello World 2\n");
    // 1号文件描述符引用stdout设备文件对象
    dup2(newfd,STDOUT_FILENO);
    printf("Hello World 3\n");
    close(fdw);
    return 0;
}

