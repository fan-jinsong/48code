#include <48func.h>
int main(int argc, char *argv[])
{
    // ./4_homework_B 1.pipe
    ARGS_CHECK(argc,2);
    int fdr = open(argv[1],O_RDONLY);
    ERROR_CHECK(fdr,-1,"open");
    char buf[4096] = {0};
    read(fdr,buf,sizeof(buf));
    printf("%s\n",buf);
    close(fdr);
    return 0;
}

