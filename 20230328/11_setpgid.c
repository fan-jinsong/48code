#include <48func.h>
int main(int argc, char *argv[])
{
    if(fork() == 0){
        printf("I am child, pid = %d, ppid = %d, pgid = %d\n",
               getpid(),getppid(),getpgid(0));
        setpgid(0,0);//子进程单独创建一个新的进程组
        printf("I am child, pid = %d, ppid = %d, pgid = %d\n",
               getpid(),getppid(),getpgid(0));
    }
    else{
        printf("I am parent, pid = %d, ppid = %d, pgid = %d\n",
               getpid(),getppid(),getpgid(0));
        wait(NULL);
    }
    return 0;
}

