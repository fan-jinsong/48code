#include <48func.h>
int main()
{
    if(fork() == 0){
        printf("I am child, pid = %d, ppid = %d\n", getpid(),getppid());
    }
    else{
        printf("I am parent, pid = %d, ppid = %d\n", getpid(),getppid());
        wait(NULL);
    }
    return 0;
}

