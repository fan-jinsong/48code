#include <48func.h>
int main(int argc, char *argv[])
{
    int shmid = shmget(IPC_PRIVATE,4096,IPC_CREAT|0600);
    ERROR_CHECK(shmid,-1,"shmget");
    int *p = (int *)shmat(shmid,NULL,0);
    ERROR_CHECK(p,(void *)-1,"shmat");
    p[0] = 0;
    if(fork() == 0){
        for(int i = 0; i < 10000000; ++i){
            ++p[0];
        }
    }
    else{
        for(int i = 0; i < 10000000; ++i){
            ++p[0];
        }
        wait(NULL);
        printf("p[0] = %d\n", p[0]);
    }
    return 0;
}

