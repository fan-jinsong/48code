#include <48func.h>
void handler(int signum){
    printf("signum = %d\n", signum);
}
int main(int argc, char *argv[])
{
    signal(SIGINT,handler);
    char buf[1024] = {0};
    read(STDIN_FILENO,buf,sizeof(buf));
    printf("buf = %s\n", buf);
    return 0;
}

