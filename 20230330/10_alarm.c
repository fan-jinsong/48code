#include <48func.h>
void handler(int signum){
    printf("signum = %d\n", signum);
    time_t now = time(NULL);
    printf("handler now = %s\n", ctime(&now));
}
int main(int argc, char *argv[])
{
    time_t now = time(NULL);
    printf("now = %s\n", ctime(&now));
    alarm(10);
    signal(SIGALRM,handler);
    while(1){
    }
    return 0;
}

