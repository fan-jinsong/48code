#include <48func.h>
void func(){
    printf("Hello\n");
    pthread_exit((void *)123);
}
void *threadFunc(void *arg){
    func();
    printf("World\n");
}
int main()
{
    pthread_t tid;
    pthread_create(&tid,NULL,threadFunc,NULL);
    void * ret;//将要获取子线程的返回值
    pthread_join(tid,&ret);
    printf("ret = %ld\n", (long )ret);
    return 0;
}

