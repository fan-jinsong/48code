#include <48func.h>
void free1(void *arg){//wrapper function
    printf("p1 is free!\n");
    free(arg);
}
void free2(void *arg){
    printf("p2 is free!\n");
    free(arg);
}
void free3(void *arg){ // free3(&fd3)
    int *pfd3 = (int *)arg;
    printf("fd3 is closed!\n");
    close(*pfd3);
}
void *threadFunc(void *arg){
    void * p1 = malloc(1024); // free 
    pthread_cleanup_push(free1,p1);
    void * p2 = malloc(1024);
    pthread_cleanup_push(free2,p2);
    int fd3 = open("file1",O_RDWR);
    pthread_cleanup_push(free3,&fd3);
    // ... 
    //pthread_exit(NULL);
    return NULL;
    //free3(&fd3);
    pthread_cleanup_pop(1);
    // free2(p2);
    pthread_cleanup_pop(1);
    // free1(p1); --> pthread_cleanup_pop
    pthread_cleanup_pop(1); // pop写一个正整数
}
int main()
{
    pthread_t tid;
    pthread_create(&tid,NULL,threadFunc,NULL);
    pthread_join(tid,NULL);
    return 0;
}

