#include <48func.h>
void *threadFunc(void *arg){
   printf("I am child thread!\n"); 
}
int main()
{
    pthread_t tid;
    pthread_create(&tid,NULL,threadFunc,NULL);
    sleep(1);
    printf("I am main thread!\n");
    return 0;
}

