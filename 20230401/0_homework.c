#include <48func.h>
int num = 0;
void *threadFunc(void *arg){
    pthread_mutex_t * pmutex = (pthread_mutex_t *)arg;
    for(int i = 0; i < 20000000; ++i){
        pthread_mutex_lock(pmutex);
        ++num;
        pthread_mutex_unlock(pmutex);
    }
    pthread_exit(NULL);
}
int main()
{
    pthread_t tid;
    pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
    struct timeval begTime,endTime;
    gettimeofday(&begTime,NULL);
    pthread_create(&tid,NULL,threadFunc,&mutex);
    for(int i = 0; i < 20000000; ++i){
        pthread_mutex_lock(&mutex);
        ++num;
        pthread_mutex_unlock(&mutex);
    }
    pthread_join(tid,NULL);
    gettimeofday(&endTime,NULL);
    printf("total time = %ld us\n", 1000000*(endTime.tv_sec - begTime.tv_sec) + endTime.tv_usec - begTime.tv_usec);
    printf("num = %d\n", num);
    return 0;
}

