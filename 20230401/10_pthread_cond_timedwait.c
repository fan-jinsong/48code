#include <48func.h>
int main()
{
    pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
    pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
    pthread_mutex_lock(&mutex);
    time_t now = time(NULL);
    printf("now = %s\n", ctime(&now));
    struct timespec abstime;
    abstime.tv_nsec = 0;
    abstime.tv_sec = now + 10;
    pthread_cond_timedwait(&cond,&mutex,&abstime);
    pthread_mutex_unlock(&mutex);
    now = time(NULL);
    printf("now = %s\n", ctime(&now));
    return 0;
}

