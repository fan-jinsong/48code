#include <48func.h>
int flag = 0; // B能否执行的条件
void *threadFunc(void *arg){
    pthread_mutex_t * pmutex = (pthread_mutex_t *)arg;
    while(1){
        pthread_mutex_lock(pmutex);
        if(flag != 0){
            pthread_mutex_unlock(pmutex);
            break;
        }
        pthread_mutex_unlock(pmutex);
    }
    printf("B is done!\n");
}
int main()
{
    pthread_t tid;
    pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
    pthread_create(&tid,NULL,threadFunc,&mutex);
    printf("A is begin!\n");
    sleep(3);
    printf("A is end!\n");
    pthread_mutex_lock(&mutex);
    flag = 1;
    pthread_mutex_unlock(&mutex);
    pthread_join(tid,NULL);
    return 0;
}

