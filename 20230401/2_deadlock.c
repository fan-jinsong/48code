#include <48func.h>
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
void *threadFunc(void *arg){
    pthread_mutex_lock(&mutex);
    printf("I am child thread!\n");
    pthread_exit(NULL);
}
int main()
{
    pthread_t tid;
    pthread_create(&tid,NULL,threadFunc,NULL);
    sleep(1);
    pthread_mutex_lock(&mutex);
    printf("I am main thread!\n");
    pthread_mutex_unlock(&mutex);
    return 0;
}

