#include <48func.h>
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
void *threadFunc(void *arg){
    sleep(1);
    while(1){
        int ret = pthread_mutex_trylock(&mutex);
        THREAD_ERROR_CHECK(ret,"trylock 2");
        if(ret == 0){
            break;
        }
    }
    sleep(5);
    printf("child thread sleep over!\n");
    pthread_mutex_unlock(&mutex);
}
int main()
{
    pthread_t tid;
    pthread_create(&tid,NULL,threadFunc,NULL);
    int ret = pthread_mutex_trylock(&mutex);
    THREAD_ERROR_CHECK(ret,"trylock 1");
    sleep(5);
    printf("main thread sleep over!\n");
    pthread_mutex_unlock(&mutex);
    pthread_join(tid,NULL);
    return 0;
}

