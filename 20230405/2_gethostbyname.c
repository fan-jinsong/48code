#include <48func.h>
int main(int argc, char *argv[])
{
    // ./2_gethostbyname www.baidu.com
    ARGS_CHECK(argc,2);
    struct hostent *p = gethostbyname(argv[1]);
    if(p == NULL){
        herror("gethostbyname");
        return -1;
    }
    printf("Host official name = %s\n", p->h_name);
    for(int i = 0; p->h_aliases[i] != NULL; ++i){
        printf("\t alias name = %s\n", p->h_aliases[i]);
    }
    printf("addrtype = %d\n", p->h_addrtype);
    printf("addrlength = %d\n",p->h_length);
    for(int i = 0;p->h_addr_list[i] != NULL; ++i){
        char buf[1024] = {0};
        inet_ntop(p->h_addrtype,p->h_addr_list[i],buf,1024);
        printf("\t addr = %s\n", buf);
    }
    return 0;
}

