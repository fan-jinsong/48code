#include <48func.h>
int main(int argc, char *argv[])
{
    // ./3_client_udpchat 192.168.118.128 1234
    ARGS_CHECK(argc,3);
    int sockfd = socket(AF_INET,SOCK_DGRAM,0);// 创建一个udp的socket
    struct sockaddr_in serveraddr;
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_port = htons(atoi(argv[2]));
    serveraddr.sin_addr.s_addr = inet_addr(argv[1]);
    // 客户端先发消息 sendto
    sendto(sockfd,"zaima",5,0,
           (struct sockaddr *)&serveraddr,sizeof(serveraddr));
    char buf[4096] = {0};
    fd_set rdset;
    while(1){
        FD_ZERO(&rdset);
        FD_SET(STDIN_FILENO,&rdset);
        FD_SET(sockfd,&rdset);
        select(sockfd+1,&rdset,NULL,NULL,NULL);
        if(FD_ISSET(sockfd,&rdset)){
            bzero(buf,sizeof(buf));
            ssize_t sret = recvfrom(sockfd,buf,sizeof(buf),0,NULL,NULL);
            if(sret == 0){
                break;
            }
            printf("buf = %s\n", buf);
        }
        if(FD_ISSET(STDIN_FILENO,&rdset)){
            bzero(buf,sizeof(buf));
            ssize_t sret = read(STDIN_FILENO,buf,sizeof(buf));
            if(sret == 0){
                sendto(sockfd,buf,0,0,
                       (struct sockaddr *)&serveraddr,
                       sizeof(serveraddr));
                break;
            }
            sendto(sockfd,buf,strlen(buf),0,
                   (struct sockaddr *)&serveraddr,
                   sizeof(serveraddr));
        }
    }
    
    close(sockfd);
    return 0;
}

