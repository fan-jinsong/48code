#include <48func.h>
//用于在master进程中存储每个worker进程的状态
enum {
    FREE,
    BUSY
};
typedef struct workerdata_s {
    pid_t pid; // 子进程的pid
    int status; // 子进程是BUSY还是FREE
} workerdata_t;
int makeWorker(int workernum, workerdata_t * workerdataArr);
