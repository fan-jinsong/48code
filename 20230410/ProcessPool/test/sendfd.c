#include <48func.h>
int main(int argc, char *argv[])
{   
    int fds[2];
    pipe(fds);
    if(fork() == 0){
        // 子进程
        //close(fds[1]);
        int fd1,fd2;
        fd2 = open("file2",O_RDWR);
        printf("child, fd2 = %d\n", fd2);
        read(fds[0],&fd1,sizeof(int));
        write(fd1,"world",5);
        close(fd2);
        close(fds[0]);
        exit(0);
    }
    else{
        // 父进程 
        //close(fds[0]);
        int fd1 = open("file1",O_RDWR);
        printf("parent, fd1 = %d\n", fd1);
        write(fd1,"hello",5);
        write(fds[1],&fd1,sizeof(int));//发送了一个文件描述符的数值
        wait(NULL);
        close(fd1);
        close(fds[1]);
    }
    return 0;
}

