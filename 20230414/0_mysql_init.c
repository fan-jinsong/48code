#include <48func.h>
#include <mysql/mysql.h>
int main()
{
    MYSQL * mysql = mysql_init(NULL); // 为mysql连接分配内存（线程不安全）
    MYSQL * ret = mysql_real_connect(mysql,"localhost","root","123","48test",0,NULL,0);
    if(ret == NULL){
        fprintf(stderr,"mysql connect error:%s\n", mysql_error(mysql));
        return -1;
    }
    char sql[1024] = "INSERT INTO event VALUES('Fluffy','1995-05-15','litter','4 kittens, 3 female, 1 male');";
    //char sql[1024] = "INSERT INTO event VALUES(Fluffy','1995-05-15','litter','4 kittens, 3 female, 1 male');";
    int qret = mysql_query(mysql,sql);
    if(qret != 0){
        fprintf(stderr,"sql error:%s\n", mysql_error(mysql));
        return -1;
    }
    mysql_close(mysql);
    return 0;
}

