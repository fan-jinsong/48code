#include <48func.h>
#include <mysql/mysql.h>
int main()
{
    MYSQL * mysql = mysql_init(NULL); // 为mysql连接分配内存（线程不安全）
    MYSQL * ret = mysql_real_connect(mysql,"localhost","root","123","48test",0,NULL,0);
    if(ret == NULL){
        fprintf(stderr,"mysql connect error:%s\n", mysql_error(mysql));
        return -1;
    }
    char sql[1024] = "select * from pet;";
    int qret = mysql_query(mysql,sql);
    if(qret != 0){
        fprintf(stderr,"sql error:%s\n", mysql_error(mysql));
        return -1;
    }
    MYSQL_RES *result = mysql_store_result(mysql); //取出读取的结果
    printf("total rows = %lu\n", mysql_num_rows(result));
    MYSQL_ROW row;
    while((row = mysql_fetch_row(result)) != NULL){
        for(unsigned int i = 0; i < mysql_num_fields(result); ++i){
            printf("%s\t", row[i]);
        }
        printf("\n");
    }
    mysql_free_result(result);
    mysql_close(mysql);
    return 0;
}

