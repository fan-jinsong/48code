#include <48func.h>
#include <openssl/md5.h>
int main()
{
    int fd = open("file1",O_RDONLY);
    char buf[4096];
    MD5_CTX c;
    MD5_Init(&c);
    while(1){
        bzero(buf,sizeof(buf));
        ssize_t sret = read(fd,buf,sizeof(buf));
        if(sret == 0){
            break;
        }
        MD5_Update(&c,buf,sret);
    }
    unsigned char md[16];//md里面存储了md5的二进制形式
    MD5_Final(md,&c);
    for(int i = 0; i < 16;++i){
        printf("%02x",md[i]);
    }
    printf("\n");
    return 0;
}

