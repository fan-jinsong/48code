#include <48func.h>
#include <l8w8jwt/encode.h>
int main(void){
    char* jwt;
    size_t jwt_length;
    struct l8w8jwt_encoding_params params;
    l8w8jwt_encoding_params_init(&params);
    // 照抄
    params.alg = L8W8JWT_ALG_HS512;
    // 自定义，保持不变
    params.sub = "Yunpan";
    params.iss = "Liao";
    params.aud = "User";
    // 时间设置弄简单一点
    params.iat = 0;
    params.exp = 0x7fffffff;
    // secret_key 就是info 每个用户的secret_key要不一样
    params.secret_key = (unsigned char*)"user1";
    params.secret_key_length = strlen(params.secret_key);
    params.out = &jwt;
    params.out_length = &jwt_length;
    int r = l8w8jwt_encode(&params);
    printf("\n l8w8jwt example HS512 token: %s \n",jwt);
    l8w8jwt_free(jwt);

    return 0;
}
